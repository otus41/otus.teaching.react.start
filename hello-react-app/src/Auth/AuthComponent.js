import React, { useEffect, useState } from "react";
import PropTypes from 'prop-types';

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [error, setError] = useState();

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await loginUser({
      login: username,
      password : password
    });
    setToken(token);

    alert("Gongratulation! You are in the system!");
  }
  
  Login.propTypes = {
    setToken: PropTypes.func.isRequired
  };

  async function loginUser(credentials) {
    console.log("Call LoginMe() method!");

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(credentials) 
    };

    return fetch(`http://localhost:5263/api/v1/auth/login`, requestOptions)
    .then(response => {

        // check for error response
        if (!response.ok) {
            // get error message from body or default to response status
            const error = response.status;
            
            if(response.status == 401)
            {
              alert("Login or password is valid! Check your credentials");
            }
        } 

        return response.json();
      })
    .catch(error => {
        setError({ errorMessage: error.toString() });
        console.error('There was an error!', error);
    });
  };

  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="form-group mt-3">
            <label>Phone number</label>
            <input
              className="form-control mt-1"
              placeholder="Enter phone number"
              onChange={e => setUserName(e.target.value)}
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              className="form-control mt-1"
              placeholder="Enter password"
              onChange={e => setPassword(e.target.value)}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
{/*           <p className="forgot-password text-right mt-2">
            Forgot <a href="#">password?</a>
          </p> */}
        </div>
      </form>
    </div>
  )
}

