import "bootstrap/dist/css/bootstrap.min.css"
import logo from './logo.svg';
import './App.css';
import HelloComponent from './HelloComponent';
import AuthComponent from './Auth/AuthComponent';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import React, { useEffect, useState } from "react";
  
function setToken(accessToken) {
  sessionStorage.setItem('accessToken', JSON.stringify(accessToken));
}

function getToken() {
  const tokenString = sessionStorage.getItem('accessToken');
  const userToken = JSON.parse(tokenString);
  return userToken?.accessToken
}

function App() {
  const token = getToken();

  if(!token) {
    return <AuthComponent setToken={setToken} />
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/auth" element={<AuthComponent />} />
        <Route path="" element={<AuthComponent />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
