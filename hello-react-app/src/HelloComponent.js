import { Component } from "react";

export default function HelloComponent (props){
    return <div>Hello, {props.name}</div>
}